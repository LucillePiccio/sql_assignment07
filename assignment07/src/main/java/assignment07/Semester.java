package main.java.assignment07;

import java.sql.*;

public class Semester {
    private String semester_id;
    private String season;
    
    //empty constructor
    public Semester(){

    }

    //default cosntructor
    public Semester(String semester_id, String season){
        this.semester_id = semester_id;
        this.season = season;
        semesterType = "SEMESTER_TYP";
    }
    
    //getter methods
    public String getSemester_id(){
        return this.semester_id;
    }

    public String getSeason(){
        return this.season;
    }

    //setter methods
    public void setSemester_id(String semester_id){
        this.semester_id = semester_id;
    }

    public void setSeason(String season){
        this.season = season;
    }

    //toString
    @Override
    public String toString(){
        return "Semester_id: " + this.semester_id + " Season: " + this.season;
    }

    //override read and write to send object to the DB
    //retrieving Semester field values using getter methods to properly store in side DB
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException{
        stream.writeString(getSemester_id());
        stream.writeString(getSeason());
    }

    //setting fields of the class Semester
    @Override void readSQL(SQLInput stream, String semesterType) throws SQLException{
        setSemester_id(stream.readString());
        setSeason(stream.readString());
    }

    //method that takes in a connection to connect to the database and add this object
    public Connection AddToDatabase(Connection conn){

        return conn;
    }
}

