package main.java.assignment07;

import java.sql.*;

public class Term implements SQLData{
    private String term_id;
    private int term_number;
    private Semester semester_id;
    private Course course_id;

    //empty constructor
    public Term(){

    }

    //default constructor
    public Term(String term_id, int term_number, Semester semester_id, Course course_id){
        this.term_id = term_id;
        this.semester_id = semester_id;
        this.course_id = course_id;
        termType = "TERM_TYP";
    }

    //getter methods
    public String getTerm_id(){
        return this.term_id;
    }

    public int getTerm_number(){
        return this.term_number;
    }

    public Semester getSemester_id(){
        return this.semester_id;
    }

    public Course getCourse_id(){
        return this.course_id;
    }

    //setter methods
    public void setTerm_id(String term_id){
        this.term_id = term_id;
    }

    public void setTerm_number(int term_number){
        this.term_number = term_number;
    }

    public void setSemester_id(Semester semester_id){
        this.semester_id = semester_id;
    }

    public void setCourse_id(Course course_id){
        this.course_id = course_id;
    }

    //override read and write to send object to the DB
    //retrieving the Term fields using the getter methods to properly store it in the DB
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException{
        stream.writeString(getTerm_id());
        stream.writeInt(getTerm_number());
        stream.writeSemester(getSemester_id());
        stream.writeCourse(getCourse_id());
    }

    //setting the fields of Term class
    @Override
    public void readSQL(SQLInput stream, String termType) throws SQLException{
        setTerm_id(stream.readString());
        setTerm_number(stream.readInt());
        setSemester_id(stream.readSemester());
        setCourse_id(stream.readCourse());
    }

    //method that takes in a connection to connect to the database and add this object
    public Connection AddToDatabase(Connection conn){

        return conn;
    }
}
