package main.java.assignment07;

import java.sql.*;

public class Hour {
    private String hour_id;
    private int total_time;
    private int class_time;
    private int lab_time;
    private int homework_time;

    //empty constructor
    public Hour(){

    }

    //default constructor
    public Hour(String house_id, int total_time, int class_time, int homework_time){
        this.hour_id = hour_id;
        this.total_time = total_time;
        this.class_time = class_time;
        this.lab_time = lab_time;
        this.homework_time = homework_time;
        HourType = "HOUR_TYP";
    }

    //getter methods
    public String getHour_id(){
        return this.hour_id;
    }

    public int getTotal_time(){
        return this.total_time;
    }

    public int getClass_time(){
        return this.class_time;
    }

    public int getLab_time(){
        return this.lab_time;
    }

    public int getHomework_time(){
        return this.homework_time;
    }

    //setter methods
    public void setHour_id(String hour_id){
        this.hour_id = hour_id;
    }

    public void setTotal_time(int total_time){
        this.total_time = total_time;
    }

    public void setClass_time(int class_time){
        this.class_time = class_time;
    }

    public void setHomework_time(int homework_time){
        this.homework_time = homework_time;
    }
    
    //override read and write to send object to the DB
    //retrieving Hour field values using getter methods to properly store in DB
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException{
        stream.writeString(getHour_id());
        stream.writeInt(getTotal_time());
        stream.writeInt(getClass_time());
        stream.writeInt(getHomework_time());
    }

    //setting fields of the class Hour
    @Override
    public void readSQL(SQLInput stream, String HourType) throws SQLException{
        setHour_id(stream.readString());
        setTotal_time(stream.readInt());
        setClass_time(stream.readInt());
        setHomework_time(stream.readInt());
    }

    //method that takes in a connection to connect to the database and add this object
    public Connection AddToDatabase(Connection conn){

        return conn;
    }
    
}
