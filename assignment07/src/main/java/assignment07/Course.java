package main.java.assignment07;

import java.sql.*;

public class Course implements SQLData {
    private String course_id;
    private String course_name;
    private String description;
    private Hour hour_id;

    //empty constructor
    public Course(){

    }

    //default constructor
    public Course(String course_id, String course_name, String description, Hour hour_id){
        this.course_id =  course_id;
        this.course_name = course_name;
        this.description = description;
        this.hour_id = hour_id;
        courseType = "COURSE_TYP";
    }

    //getter methods
    public String getCourse_id(){
        return this.course_id;
    }

    public String getCourse_name(){
        return this.course_name;
    }

    public String getDescription(){
        return this.description;
    }

    public Hour getHour_id(){
        return this.hour_id;
    }

    //setter methods
    public void setCourse_id(String course_id){
        this.course_id = course_id;
    }

    public void setCourse_name(String course_name){
        this.course_name = course_name;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setHour_id(Hour hour_id){
        this.hour_id = hour_id;
    }
 
    //override read and write to send object to the DB
    //taking the course field values and storing it properly inside the DB
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException{
        stream.writeString(getCourse_id());
        stream.writeString(getCourse_name());
        stream.writeString(getDescription());
        stream.writeHour(getHour_id());
    }

    //setting the fields of Course class
    @Override
    public void readSQL(SQLInput stream, String courseType) throws SQLException{
        setCourse_id(stream.readString());
        setCourse_name(stream.readString());
        setDescription(stream.readString());
        setHour_id(stream.readHour());
    }

    //method that takes in a connection to connect to the database and add this object
    public Connection AddToDatabase(Connection conn){

    return conn;
    }

}
