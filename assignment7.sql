--Assignment7
--creating tables from the ERD
CREATE TABLE Semester(
    Semester_id     VARCHAR2(5) PRIMARY KEY,
    Season          VARCHAR2(7) NOT NULL
);

CREATE TABLE Hour(
    Hour_id         VARCHAR2(5) PRIMARY KEY,
    Total_time      NUMBER(2)   NOT NULL,
    Class_time      NUMBER(2)   NOT NULL,
    Lab_time        NUMBER(2)   NOT NULL,
    Homework_time   NUMBER(2)   NOT NULL
);

CREATE TABLE Course(
    Course_id   VARCHAR2(11)  PRIMARY KEY,
    Course_name VARCHAR2(100) NOT NULL,
    Description VARCHAR2(500) NOT NULL,
    Hour_id     VARCHAR2(5),
    
    CONSTRAINT Course_Hour_id_FK FOREIGN KEY (Hour_id) REFERENCES Hour(Hour_id)
);

CREATE TABLE Term(
    Term_id     VARCHAR2(5) PRIMARY KEY,
    Term_number NUMBER(1)   NOT NULL,
    Semester_id VARCHAR2(5),
    Course_id   VARCHAR2(11),
    
    CONSTRAINT Term_Semester_id_FK  FOREIGN KEY (Semester_id)   REFERENCES Semester(Semester_id),
    CONSTRAINT Term_Course_id_FK    FOREIGN KEY (Course_id)     REFERENCES Course(Course_id)
);

--Dropping tables if needed
--DROP TABLE Term;
--DROP TABLE Course;
--DROP TABLE Hour;
--DROP TABLE Semester;

--inserting data of programming1 and programming2
--programming1
INSERT INTO Semester(Semester_id, Season)
VALUES('S0001', 'Fall');

INSERT INTO Hour(Hour_id, Total_time, Class_time, Lab_time, Homework_time)
VALUES('H0001', 90, 3, 3, 3);

INSERT INTO Course(Course_id, Course_name, Description, Hour_id)
VALUES('420-110-DW', 'Programming 1', 'The course will introduce the student to the basic building blocks (sequential, selection and repetitive control strutures) and modules(methods and classes) used to write a program. The student will use the java Programming language to implement the algorithms studied. The array data structure is intoduced, and student will learn how to program with objects', 'H0001');

INSERT INTO Term(Term_id, Term_number, Semester_id, Course_id)
VALUES('Te001', 1, 'S0001', '420-110-DW');

--programming2
INSERT INTO Semester(Semester_id, Season)
VALUES('S0002', 'Winter');

INSERT INTO Hour(Hour_id, Total_time, Class_time, Lab_time, Homework_time)
VALUES('H0002', 90 , 3, 3, 3); 

INSERT INTO Course(Course_id, Course_name, Description, Hour_id)
VALUES('420-210-DW', 'Programming 2','The course wil introduce the student to basic object-oriented methodology in order to design, implemnt, use and modify classes, to write programs in the Java language that perform interactice processing, array and string processing, and data validation. Object-oriented features such as encapsulation and inheritance will be explored.', 'H0002');

INSERT INTO Term(Term_id, Term_number, Semester_id, Course_id)
VALUES('Te002', 2, 'S0002', '420-210-DW');

COMMIT;

--Creating objects here, procedures to insert in tables, functions to get back data
CREATE TYPE SemesterType AS OBJECT(
    Semester_id VARCHAR2(5),
    Season      VARCHAR2(7)
);
/

CREATE OR REPLACE PROCEDURE addSemester(vSemester IN SemesterType) 
AS
BEGIN
    INSERT INTO Semester
    VALUES(vSemester.Semester_id, vSemester.Season);
END;
/

CREATE OR REPLACE FUNCTION getSemester(vSemester_id IN Semester.Semester_Id%TYPE)
RETURN SemesterType AS
    vSemesterRow Semester%ROWTYPE;
    vSemester SemesterType;         
BEGIN
    SELECT * INTO vSemester
    FROM Semester S
    WHERE S.Semester_id = vSemester_id;
    
    RETURN SemesterType(vSemesterRow.Semester_id, vSmesterRow.Season);
END;
/

CREATE TYPE HourType AS OBJECT(
    Hour_id VARCHAR2(5), 
    Total_time NUMBER(2),
    Class_time NUMBER(1),
    Lab_time NUMBER(1),
    Homework_time NUMBER(1)
);
/

CREATE OR REPLACE PROCEDURE addHour(vHour IN HourType) AS
BEGIN
    INSERT INTO Hour
    VALUES(vHour.Hour_id, vHour.Total_time, vHour.Class_time, vHour.Lab_time, vHour.Homework_time);
END;
/

CREATE OR REPLACE FUNCTION getHour(vHour IN Hour.Hour_id%TYPE)
RETURN HourType
AS
    vHourRow Hour%ROWTYPE;
    vHour HourType;
BEGIN
    SELECT * INTO vHourRow
    FROM Hour H
    WHERE H.Hour_id = vHour;
    
    RETURN HourType(vHourRow.Hour_id, vHourRow.Total_time, vHourRow.Class_time, vHourRow.Lab_time, vHourRow.Homework_time);
END;
/

CREATE TYPE CourseType AS OBJECT(
    Course_id VARCHAR2(11),
    Course_name VARCHAR2(20),
    Description VARCHAR2(100),
    Hour_id VARCHAR2(5)
);
/

CREATE OR REPLACE PROCEDURE addCourse(vCourse IN CourseType)
AS
BEGIN
    INSERT INTO Course 
    VALUES(vCourse.Course_id, vCourse.Course_name, vCourse.Description, vCourse.Hour_id);
END;
/

CREATE OR REPLACE FUNCTION getCourse(vCourse_id IN Course.Course_id%TYPE)
RETURN CourseType
AS 
    vCourseRow Course%ROWTYPE;
    vCourse CourseType;
BEGIN
    SELECT * INTO vCourseRow
    FROM Course C
    WHERE C.Course_id = vCourse_id;
    
    RETURN CourseType(vCourseRow.Course_id, vCourseRow.Course_name, vCourseRow.Description, vCourseRow.Hour_id);
END;
/

CREATE TYPE TermType AS OBJECT(
    Term_id VARCHAR2(5),
    Term_number NUMBER(1),
    Semester_id VARCHAR2(5),
    Course_id VARCHAR2(11)
);
/

CREATE OR REPLACE PROCEDURE addTerm(vTerm IN TermType)
AS
BEGIN
    INSERT INTO Term
    VALUES(vTerm.Term_id, vTerm.Term_number, vTerm.Semester_id, vTerm.Course_id);
END;
/

CREATE OR REPLACE FUNCTION getTerm(vTerm_id IN Term.Term_id%TYPE)
RETURN TermType
AS
    vTermRow Term%ROWTYPE;
    vTerm TermType;
BEGIN
    SELECT * INTO vTermRow
    FROM Term T
    WHERE T.Term_id = vTerm_id;
    
    RETURN TermType(vTermRow.Term_id, vTermRow.Term_number, vTermRow.Semester_id, vTermRow.Course_id);
END;
/